<?php 

function smug_settings_form($form, &$form_state)
{
	$settings = SmugSettings::instance();
	
	$form['fieldset_general'] = array
	(
		'#type' => 'fieldset',
		'#title' => t('General Settings'),
		'#collapsed' => TRUE, 
	);

	$form['fieldset_general']['api_key'] = array
	(
		'#type' => 'textfield', 
		'#title' => t('API Key'),
		'#required' => TRUE,
		'#default_value' => $settings->api_key,
	);

	$form['fieldset_general']['secret_key'] = array
	(
		'#type' => 'textfield', 
		'#title' => t('OAuth Secret Key'),
		'#required' => TRUE,
		'#default_value' => $settings->secret_key,
	);

	$form['fieldset_general']['result_count'] = array
	(
		'#type' => 'textfield', 
		'#title' => t('Number of result diplayed.'),
		'#required' => TRUE,
		'#default_value' => $settings->result_count,
	);

	$form['submit'] = array
	(
		'#type' => 'submit',
		'#value' => t('Save settings')
	);

	return $form;
}

function smug_settings_form_validate($form, &$form_state)
{
	$f = $form_state['values'];

	if(! filter_var($f['result_count'], FILTER_VALIDATE_INT) || $f['result_count'] < 1)
	{
		form_set_error('result_count', t('The result count should be numeric and not less than 1.'));
	}
}

function smug_settings_form_submit($form, &$form_state)
{
	$f = $form_state['values'];

	unset($f['submit'], $f['form_build_id'], $f['form_token'], $f['form_id'], $f['op']);

	if(SmugSettings::instance()->save($f))
	{
		drupal_set_message(t('Smug settings updated..'), 'status');
	}
}	