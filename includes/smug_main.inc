<?php

/**
 * this receiveds on the request from js
 * 
 * @param  string $load the method to execute
 * @param  integer $id  album id
 * @param  string $key  album key
 * @return string       
 */
function smug_main($load, $id = null, $key = null)
{
	$smug = new Smug;
	
	if($smug->is_auth())
	{
		switch ($load) 
		{
			case 'album': smug_album($smug, $id, $key); break;
			case 'albums': smug_albums($smug); break;
			case 'cc': smug_cc($smug); break;
			case 'data': smug_data($smug); break;
			case 'download': smug_download(); break;
		}
	}
	else
	{
		echo '<p>You need to authorize this app to be able to access your smugmug account. when done just click refresh.</p>';
		echo '<a target="_blank" href="'.$smug->get_auth_url("Access=[Public|Full]", "Permissions=[Read|Add|Modify]").'">Authorize</a>';
		echo '<br />';
		echo '<a href="http://test-smug.ph/admin/smug/main">Refresh</a>';
	}

	die;
}

/**
 * download image
 * 
 * @return string json encoded string that contains the download status and donwloaded image path.
 */
function smug_download()
{
	// url passed from js function.
	$url = $_GET['url'];

	// download data.
	$data = file_get_contents($url);

	// store to tmp directory.
	$filename = sys_get_temp_dir().'/'.basename($url);

	// create destination directory
	if(! file_exists('public://smug/'))
	{
		mkdir('public://smug/', 0777, true);
	}

	$destination = 'public://smug/'.basename($url);

	$response = array(
		'destination' => drupal_realpath($destination),
		'status' => 0
	);

	if(file_put_contents($destination, $data))
	{
		$response['message'] = 'Image downloaded successfuly and ready for upload.';
		$response['status'] = 1;
	}
	else
	{
		$response['message'] = 'Failed to download image.';
	}

	echo json_encode($response);

	die;
}

/**
 * clear cache
 * @param  Smug $smug instance of Smug class
 * @return string
 */
function smug_cc($smug)
{
	echo $smug->clear_cache(); 

	die;
}

/**
 * album and images details
 * 
 * @param  Smug $smug instance of the Smug class
 * @return string       json encoded string that contains album and album images.
 */
function smug_data($smug)
{
	$data = array();

	$albums = $smug->get_albums();

	if(! empty($albums))
	{
		foreach($albums as $album)
		{
			$images = $smug->get_images($album['id'], $album['Key']);

			$i = array();

			if(isset($images['Images']) and ! empty($images['Images']))
			{
				foreach($images['Images'] as $image)
				{
					$i[$image['id']] = $image;
				}
			}

			$data[ $album['id'] ] = $album + array('images' => $i);
		}
	}

	echo json_encode($data);

	die;
}


function smug_album($smug, $id, $key) {

	$images = $smug->get_images($id, $key);

?>
	<div class="container">
		<h2>Select Image</h2>

		<form id="smug-form">
			<div class="smug-form-filter">
				<input id='filter' type="text" />
				<input type="button" onclick="search();" value="search" /> 
				<a href='#' onclick="open_smug();">back to albums</a>
			</div>
		</form>

		<div id="result"></div>

		<div class="download-status"></div>

		<?php if($images) : ?>
			<div id='smug-images' class="sixteen columns">
			<?php foreach($images['Images'] as $image) : ?>
					<div class="image three columns">
						<div class="">
							<a href="#" onclick="download_smug_image(<?php echo $id; ?>, <?php echo $image[id] ?>)">
								<img src="<?php echo $image['TinyURL']; ?>" />
							</a>

							<div class="image-data">
								<span class='image-data-caption'><?php echo $image['Caption'] ?></span>
								<span class='image-data-filename'><?php echo $image['FileName'] ?></span>
								<span class='image-data-keywords'><?php echo $image['Keywords'] ?></span>
							</div>
						</div>
					</div>
				<?php endforeach; ?>
			</div>
		<?php endif; ?>	
	</div>

	<div id="page-selection"></div>
<?php 
}

function smug_albums($smug) {

	$albums = $smug->get_albums();
?>
	<div class="container">
		<h2>Select Album</h2>

		<?php if($albums) : ?>
			<div class="sixteen columns">
				
				<?php foreach($albums as $album) : ?>
					<div class="three columns">
						<div class="album">
							<a href="#" onclick="open_smug_album('<?php echo $album['id']; ?>', '<?php echo $album['Key']; ?>')">
								<img src="<?php echo $GLOBALS['base_url'].'/'.drupal_get_path('module', 'smug').'/assets/images/album.png'; ?>" />
								<div class='title'><?php echo $album['Title']; ?></div>
							</a>
						</div>
					</div>
				<?php endforeach; ?>

			</div>
		<?php endif; ?>

		<!-- <div class=""><a href='#' onclick="cc_smug();">clear cache</a></div> -->
	</div>	
<?php 
}
?>

