<?php 
/**
 * Smug Settings Class.
 */
class SmugSettings {

	protected static $instance;

	protected $settings;

	public static function instance()
	{
		if(is_null(self::$instance))
		{
			self::$instance = new SmugSettings;
		}

		return self::$instance;
	}

	protected function __construct()
	{
		$query = db_select(SmugContants::SETTINGS_TABLE, 's')->fields('s', $this->_fields());

		foreach($query->execute() as $settings)
		{
			$this->settings[$settings->name] = $settings->value;
		}
	}

	public function save($values)
	{
		foreach($values as $name => $value)
		{
			db_update(SmugContants::SETTINGS_TABLE)
				->fields(array('value' => $value))
				->condition('name', $name, '=')
				->execute();
		}

		return TRUE;
	}

	public function __get($key)
	{
		return isset($this->settings[$key]) ? $this->settings[$key] : null;
	}

	private function _fields()
	{
		return array('name', 'value');
	}
}