<?php 
/**
 * wrapper class for the phpSmug wrapper class. :D 
 */
class Smug {

	private $_phpsmug;

	public function __construct()
	{
		$settings = $settings = SmugSettings::instance();

		$this->_phpsmug = new phpSmug(array(
			'APIKey' => $settings->api_key,
			'OAuthSecret' => $settings->secret_key,
		));

		// enable file system caching
		$this->_phpsmug->enableCache(array(
			'type' => 'fs',
			'cache_dir' => sys_get_temp_dir(),
			'cache_expire' => 300
		));
	}


	/**
	 * check if authorized
	 * 
	 * @return boolean 
	 */
	public function is_auth()
	{
		if(isset($_SESSION['fond-smug']))
		{
			$token = unserialize($_SESSION['fond-smug']);

			$this->set_auth_token($token);

			return true;
		}

		if(isset($_SESSION['SmugGalReqToken']))
		{
			$request_token = unserialize($_SESSION['SmugGalReqToken']);

			unset($_SESSION['SmugGalReqToken']);

			$this->set_auth_token($request_token);

			$token = $this->_phpsmug->auth_getAccessToken();

			$_SESSION['fond-smug'] = serialize($token);

			$this->set_auth_token($token);

			return true;
		}

		return false;
	}

	/**
	 * authentication url.
	 * 
	 * @return string 
	 */
	public function get_auth_url()
	{
		$_SESSION['SmugGalReqToken'] = serialize($this->_phpsmug->auth_getRequestToken());

		return $this->_phpsmug->authorize("Access=[Public|Full]", "Permissions=[Read|Add|Modify]");
	}

	public function set_auth_token($token)
	{
		$this->_phpsmug->setToken(array(
			'id' => $token['Token']['id'], 
			'Secret' => $token['Token']['Secret'], 
		));
	}

	public function auth_revoke()
	{
		unset($_SESSION['SmugGalReqToken'], $_SESSION['fond-smug']);
	}

	/**
	 * get albums
	 * 
	 * @return array array of albums
	 */
	public function get_albums()
	{
		return $this->_phpsmug->albums_get(array(
			'Heavy' => 1
		));
	}

	/**
	 * get images in album
	 * 
	 * @param integer $id album id
	 * @param string $key album key
	 * @return array array of images
	 */
	public function get_images($id, $key)
	{
		return $this->_phpsmug->images_get(array(
			'AlbumID' => $id,
			'AlbumKey' => $key,
			'Heavy' => 1,
		));
	}

	/**
	 * @see phpSmug::clearCache
	 */
	public function clear_cache()
	{
		return $this->_phpsmug->clearCache(true);
	}

	public function test()
	{
		return $this->_phpsmug->getParams();
	}
}