j191 = jQuery.noConflict(true);

var smug_data = {}, smug_images = [];

function smug()
{
	j191.ajax({
		url: '/admin/smug/main/data',
		dataType: 'json',
		success: function(data){
			window.smug_data = data;
			j191.colorbox({
				href:'/admin/smug/main/albums',
				onComplete: function(){
					j191.colorbox.resize();
				}
			});
		}
	});
}

function cc_smug()
{
	j191.colorbox({
		href:'/admin/smug/main/cc',
		onComplete: function(r){
			alert('cache cleared');
			open_smug();
		}
	});
}

function open_smug()
{
	j191.colorbox({
		href:'/admin/smug/main/albums',
		onComplete: function(){
			j191.colorbox.resize();
		}
	});
}

function open_smug_album(id, key)
{
	j191.colorbox({
		href: '/admin/smug/main/album/'+id+'/'+key,
		onComplete: function(){
			window.smug_images = j191('#smug-images > .image');

			search();

			j191.colorbox.resize();
		}
	});
}

function download_smug_image(album_id, image_id)
{	
	var image = smug_data[album_id]['images'][image_id];

	j191('.download-status').addClass('spinner').html('Downloading image..');

	j191.ajax({
		url: '/admin/smug/main/download/',
		data: {'url': image['XLargeURL']},
		dataType: 'json',
		success: function(data){
			if(data.status == 1){
				j191('input[name="smug_image_path"]').val(data.destination);

				var details = j191('#smug_image_details');
				var img = j191('<img>').attr({'src': image['TinyURL'], 'class':'smug_image'});
				var info = j191('<ul>').attr('class', 'smug_image_info');

				info.append( j191('<li>').html('<span class="label">Caption: </span>'+image['Caption']) );
				info.append( j191('<li>').html('<span class="label">Image URL: </span><a href='+image['URL']+'>'+image['URL']+'</a>') );

				details.children().remove();
				details.append(img).append(info);
			}

			alert(data.message);

			j191.colorbox.close();
		}
	});

	j191.colorbox.resize();
}

function search()
{
	var images = j191(smug_images),
		filter = j191('#filter'),
		totalResults = 0,
		perPage = parseInt(j191('input[name="smug_result_count"]').val()), 
		pageNumber = 0,
		pageChunk = {}, 
		list = [];

	var _display = function(l){
		j191('#smug-images .image').remove();

		for (var i=0;i<=l.length;i++) {
			j191('#smug-images').append(l[i]);
		}
	};

	j191('#smug-images .image').remove();

	images.each(function(){
		var e = j191(this),
			c = j191('.image-data-caption', e),
			f = j191('.image-data-filename', e),
			k = j191('.image-data-keywords', e);

		caption = (filter.val().length != 0) && (c.filter(':contains("'+filter.val()+'")').length == 0);
		filename = (filter.val().length != 0) && (f.filter(':contains("'+filter.val()+'")').length == 0);
		keywords = (filter.val().length != 0) && (k.filter(':contains("'+filter.val()+'")').length == 0);

		if(caption && filename && keywords){
			e.remove();
		}
		else {
			list.push(e);
			totalResults++;
		}
	});

	for (var i=0,j=list.length; i<=j; i+=perPage) {
		pageNumber++; pageChunk[pageNumber] = list.slice(i,i+perPage);
	}

	_display(pageChunk[1]);

	j191('#result').text('Displaying of 1-'+perPage+' of '+totalResults+' result(s)');

	j191('#page-selection').unbind().remove(); 
	j191('<div id="page-selection">').insertAfter('#smug-images');
	j191('#page-selection').bootpag({total: pageNumber, maxVisible: 10}).on("page", function(event, num){ 
		_display(pageChunk[num]); 

		tail = parseInt(perPage) * num;

		base = (tail - parseInt(perPage)) + 1;
	
		if(tail > totalResults){ tail = totalResults;}
	
		j191('#result').text('Displaying of '+base+'-'+tail+' of '+totalResults+' result(s)');

		j191.colorbox.resize(); 
	});
}

(function($){

$(document).ready(function(){
	$('a#smug').click(function(e){
		e.preventDefault();
		smug();
		return;
	});
});
  
})(j191);